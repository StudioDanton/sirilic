# -*- coding: UTF-8 -*-
# ==============================================================================

# ==============================================================================
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ==============================================================================
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ==============================================================================
import sys
import wx
from sirilic.lib.constantes import IMAGE,OFFSET,DARK,FLAT,DFLAT

# ==============================================================================
class TabProperties:
    def __init__(self, i_gui, i_prefs ):

        self.i_prop = i_gui.wproperties
        self.prefs  = i_prefs

        size = self.i_prop.e_RejH_Image.GetSizeFromTextSize(self.i_prop.e_RejH_Image.GetTextExtent('0'))
        if sys.platform.startswith('win32') :
            size = self.i_prop.e_RejH_Image.GetSizeFromTextSize(self.i_prop.e_RejH_Image.GetTextExtent('0,000'))
            
        self.i_prop.e_RejH_Image.SetMinSize(size)
        self.i_prop.e_RejL_Image.SetMinSize(size)
        self.i_prop.e_RejH_Cosmetic.SetMinSize(size)
        self.i_prop.e_RejL_Cosmetic.SetMinSize(size)
        self.i_prop.e_sigma_findstar.SetMinSize(size)
        self.i_prop.e_roundness_findstar.SetMinSize(size)
        self.i_prop.e_RejH_Offset.SetMinSize(size)
        self.i_prop.e_RejL_Offset.SetMinSize(size)
        self.i_prop.e_RejH_Flat.SetMinSize(size)
        self.i_prop.e_RejL_Flat.SetMinSize(size)
        self.i_prop.e_RejH_Dark.SetMinSize(size)
        self.i_prop.e_RejL_Dark.SetMinSize(size)
        self.i_prop.e_RejH_DFlat.SetMinSize(size)
        self.i_prop.e_RejL_DFlat.SetMinSize(size)
        self.i_prop.spDegree.SetMinSize(size)

        size = self.i_prop.e_fwhm_filter.GetSizeFromTextSize(self.i_prop.e_fwhm_filter.GetTextExtent('000%'))
        self.i_prop.e_fwhm_filter.SetMinSize(size)
        self.i_prop.e_wfwhm_filter.SetMinSize(size)
        self.i_prop.e_round_filter.SetMinSize(size)
        self.i_prop.e_quality.SetMinSize(size)
        
        self.i_prop.e_minpairs.SetMinSize(size)

        size = self.i_prop.TitreOptDFlat.GetTextExtent('T')
        size = ( -1, int(1.5*size[1]) )
        self.i_prop.TitreOptImage.SetMinSize(size)
        self.i_prop.TitreOptOffset.SetMinSize(size)
        self.i_prop.TitreOptDark.SetMinSize(size)
        self.i_prop.TitreOptFlat.SetMinSize(size)
        self.i_prop.TitreOptDFlat.SetMinSize(size)

        self.i_prop.cb_StackImage.Bind(wx.EVT_COMBOBOX,  lambda evt : self.OnCHangeStack( evt, self.i_prop.cb_StackImage , self.i_prop.cb_rejtypeImage ,self.i_prop.e_RejH_Image ,self.i_prop.e_RejL_Image) )
        self.i_prop.cb_StackOffset.Bind(wx.EVT_COMBOBOX, lambda evt : self.OnCHangeStack( evt, self.i_prop.cb_StackOffset, self.i_prop.cb_rejtypeOffset,self.i_prop.e_RejH_Offset,self.i_prop.e_RejL_Offset) )
        self.i_prop.cb_StackDark.Bind(wx.EVT_COMBOBOX,   lambda evt : self.OnCHangeStack( evt, self.i_prop.cb_StackDark  , self.i_prop.cb_rejtypeDark  ,self.i_prop.e_RejH_Dark  ,self.i_prop.e_RejL_Dark) )
        self.i_prop.cb_StackFlat.Bind(wx.EVT_COMBOBOX,   lambda evt : self.OnCHangeStack( evt, self.i_prop.cb_StackFlat  , self.i_prop.cb_rejtypeFlat  ,self.i_prop.e_RejH_Flat  ,self.i_prop.e_RejL_Flat) )
        self.i_prop.cb_StackDFlat.Bind(wx.EVT_COMBOBOX,  lambda evt : self.OnCHangeStack( evt, self.i_prop.cb_StackDFlat , self.i_prop.cb_rejtypeDFlat ,self.i_prop.e_RejH_DFlat ,self.i_prop.e_RejL_DFlat) )

        self.i_prop.cb_rejtypeImage.Bind(  wx.EVT_COMBOBOX, lambda evt : self.OnCHangeRejType( evt, self.i_prop.cb_rejtypeImage ,
                                                                                                    self.i_prop.lHighImage , self.i_prop.e_RejH_Image ,
                                                                                                    self.i_prop.lLowImage  , self.i_prop.e_RejL_Image ) )
        self.i_prop.cb_rejtypeOffset.Bind( wx.EVT_COMBOBOX, lambda evt : self.OnCHangeRejType( evt, self.i_prop.cb_rejtypeOffset,
                                                                                                    self.i_prop.lHighOffset, self.i_prop.e_RejH_Offset,
                                                                                                    self.i_prop.lLowOffset , self.i_prop.e_RejL_Offset ) )
        self.i_prop.cb_rejtypeDark.Bind(   wx.EVT_COMBOBOX, lambda evt : self.OnCHangeRejType( evt, self.i_prop.cb_rejtypeDark,
                                                                                                    self.i_prop.lHighDark, self.i_prop.e_RejH_Dark ,
                                                                                                    self.i_prop.lLowDark , self.i_prop.e_RejL_Dark ) )
        self.i_prop.cb_rejtypeFlat.Bind(   wx.EVT_COMBOBOX, lambda evt : self.OnCHangeRejType( evt, self.i_prop.cb_rejtypeFlat  ,
                                                                                                    self.i_prop.lHighFlat , self.i_prop.e_RejH_Flat,
                                                                                                    self.i_prop.lLowFlat  , self.i_prop.e_RejL_Flat) )
        self.i_prop.cb_rejtypeDFlat.Bind(  wx.EVT_COMBOBOX, lambda evt : self.OnCHangeRejType( evt, self.i_prop.cb_rejtypeDFlat ,
                                                                                                    self.i_prop.lHighDFlat , self.i_prop.e_RejH_DFlat ,
                                                                                                    self.i_prop.lLowDFlat , self.i_prop.e_RejL_DFlat ) )

        self.i_prop.label_weighted_offset.Show(False)
        self.i_prop.label_weighted_dark.Show(False)
        self.i_prop.label_weighted_flat.Show(False)
        self.i_prop.label_weighted_dflat.Show(False)

        self.i_prop.cb_weightedOffset.Show(False)
        self.i_prop.cb_weightedDark.Show(False)
        self.i_prop.cb_weightedFlat.Show(False)
        self.i_prop.cb_weightedDFlat.Show(False)

    # --------------------------------------------------------------------------
    def OnCHangeStack(self, event, stack, rejtype, rejH, rejL):
        _unused_ = ( event ) # unused parameter , suppress pylint warning
        value = stack.GetValue()
        bValid =  value == "rej" or value == "mean"
        rejtype.Enable(bValid)
        rejH.Enable(bValid)
        rejL.Enable(bValid)

    def OnCHangeRejType(self, event, rejtype, lrejH, rejH, lrejL,rejL ):
        _unused_ = ( event ) # unused parameter , suppress pylint warning
        htext=_("High")
        ltext=_("Low")
        digits=1
        vmax=10.0
        vmin=0.1
        vdefaultH = 3.0
        vdefaultL = 3.0
        if rejtype.GetValue() == "Generalized":
            htext=_("Outliers")
            ltext=_("Significance")
            digits=3
            vmax=0.999
            vmin=0.001
            vdefaultH = 0.300
            vdefaultL = 0.050
        if rejtype.GetValue() == "Percentile":
            digits=3
            vmax=0.999
            vmin=0.001
            vdefaultH = 0.200
            vdefaultL = 0.100

        rejL.SetDigits(digits)
        rejH.SetDigits(digits)

        rejL.SetMax(vmax)
        rejH.SetMax(vmax)

        rejL.SetMin(vmin)
        rejH.SetMin(vmin)

        rejH.SetValue(vdefaultH)
        rejL.SetValue(vdefaultL)

        lrejH.SetLabel(htext+": ")
        lrejL.SetLabel(ltext+": ")
        lrejH.GetParent().Layout()


    # display message if IsSameProperties == True
    def DisplayMode(self, IsSameProperties ):
        if IsSameProperties :
            self.i_prop.l_mode_param.Show()
        else:
            self.i_prop.l_mode_param.Hide()

    # --------------------------------------------------------------------------
    # charge les proprietes dans l'onglet
    def Set(self,iodf, IsSameProperties ):
        self.DisplayMode(IsSameProperties)
        # Images Properties
        img_prop = iodf['Images'][IMAGE].GetData()
        self.i_prop.cb_OffsetSub_Image.SetValue(img_prop["suboffset"])
        self.i_prop.cb_optimdark.SetValue(      img_prop["DarkOpt"  ])
        self.i_prop.cb_StackImage.SetValue(     img_prop["Stack"    ])
        self.i_prop.cb_rejtypeImage.SetValue(   img_prop["RejType"  ])
        self.i_prop.e_RejH_Image.SetValue(      img_prop["RejH"     ])
        self.i_prop.e_RejL_Image.SetValue(      img_prop["RejL"     ])
        self.i_prop.cb_weightedImage.SetValue(  img_prop["Weighted" ])
        self.i_prop.cb_NormImage.SetValue(      img_prop["Norm"     ])
        self.i_prop.e_fwhm_filter.SetValue(     img_prop["fwhm"     ])
        self.i_prop.e_wfwhm_filter.SetValue(    img_prop["wfwhm"    ])
        self.i_prop.e_round_filter.SetValue(    img_prop["round"    ])
        self.i_prop.e_quality.SetValue(         img_prop["quality"  ])
        self.i_prop.cb_Cosmetic_Image.SetValue( img_prop["Cosmetic" ])
        self.i_prop.e_RejH_Cosmetic.SetValue(   img_prop["Hot"      ])
        self.i_prop.e_RejL_Cosmetic.SetValue(   img_prop["Cold"     ])
        self.i_prop.cb_findstar.SetValue(       img_prop["Findstar" ])
        self.i_prop.e_sigma_findstar.SetValue(  img_prop["Ksigma"   ])
        self.i_prop.e_roundness_findstar.SetValue( img_prop["Roundness" ])
        self.i_prop.spDegree.SetValue(          img_prop["Degree"   ])
        self.i_prop.cb_fix_fujix.SetValue(      img_prop["fix_fujix"])
        self.i_prop.cb_transf.SetValue(         img_prop["Transf"   ])
        self.i_prop.cb_norot.SetValue(          img_prop["NoRot"    ])
        self.i_prop.cb_drizzle.SetValue(        img_prop["Drizzle"  ])
        self.i_prop.e_minpairs.SetValue(        img_prop["MinPairs" ])
        # Offsets Properties
        offset_prop = iodf['Images'][OFFSET].GetData()
        self.i_prop.cb_cpylibOffset.SetValue(   offset_prop["copylib" ])
        self.i_prop.cb_StackOffset.SetValue(    offset_prop["Stack"   ])
        self.i_prop.cb_rejtypeOffset.SetValue(  offset_prop["RejType" ])
        self.i_prop.e_RejH_Offset.SetValue(     offset_prop["RejH"    ])
        self.i_prop.e_RejL_Offset.SetValue(     offset_prop["RejL"    ])
        self.i_prop.cb_weightedOffset.SetValue( offset_prop["Weighted"])
        self.i_prop.cb_NormOffset.SetValue(     offset_prop["Norm"    ])
        # Darks Properties
        dark_prop = iodf['Images'][DARK].GetData()
        self.i_prop.cb_OffsetSub_Dark.SetValue( dark_prop['suboffset'])
        self.i_prop.cb_cpylib_Dark.SetValue(    dark_prop['copylib'  ])
        self.i_prop.cb_StackDark.SetValue(      dark_prop['Stack'    ])
        self.i_prop.cb_rejtypeDark.SetValue(    dark_prop["RejType"  ])
        self.i_prop.e_RejH_Dark.SetValue(       dark_prop['RejH'     ])
        self.i_prop.e_RejL_Dark.SetValue(       dark_prop['RejL'     ])
        self.i_prop.cb_weightedDark.SetValue(   dark_prop["Weighted" ])
        self.i_prop.cb_NormDark.SetValue(       dark_prop['Norm'     ])
        # Flats Properties
        flat_prop = iodf['Images'][FLAT].GetData()
        self.i_prop.cb_OffsetSub_Flat.SetValue( flat_prop['suboffset'])
        self.i_prop.cb_cpylib_Flat.SetValue(    flat_prop['copylib'  ])
        self.i_prop.cb_StackFlat.SetValue(      flat_prop['Stack'    ])
        self.i_prop.cb_rejtypeFlat.SetValue(    flat_prop["RejType"  ])
        self.i_prop.e_RejH_Flat.SetValue(       flat_prop['RejH'     ])
        self.i_prop.e_RejL_Flat.SetValue(       flat_prop['RejL'     ])
        self.i_prop.cb_weightedFlat.SetValue(   flat_prop["Weighted" ])
        self.i_prop.cb_NormFlat.SetValue(       flat_prop['Norm'     ])
        # Dark-Flats Properties
        dflat_prop = iodf['Images'][DFLAT].GetData()
        self.i_prop.cb_OffsetSub_DFlat.SetValue(dflat_prop['suboffset'])
        self.i_prop.cb_cpylib_DFlat.SetValue(   dflat_prop['copylib'  ])
        self.i_prop.cb_StackDFlat.SetValue(     dflat_prop['Stack'    ])
        self.i_prop.cb_rejtypeDFlat.SetValue(   dflat_prop["RejType"  ])
        self.i_prop.e_RejH_DFlat.SetValue(      dflat_prop['RejH'     ])
        self.i_prop.e_RejL_DFlat.SetValue(      dflat_prop['RejL'     ])
        self.i_prop.cb_weightedDFlat.SetValue(  dflat_prop["Weighted" ])
        self.i_prop.cb_NormDFlat.SetValue(      dflat_prop['Norm'     ])

        evt = wx.CommandEvent()
        evt.SetEventType(wx.EVT_COMBOBOX.typeId)
        wx.PostEvent(self.i_prop.cb_rejtypeImage ,evt)
        wx.PostEvent(self.i_prop.cb_rejtypeOffset,evt)
        wx.PostEvent(self.i_prop.cb_rejtypeDark  ,evt)
        wx.PostEvent(self.i_prop.cb_rejtypeFlat  ,evt)
        wx.PostEvent(self.i_prop.cb_rejtypeDFlat ,evt)

    # --------------------------------------------------------------------------
    # recupere les proprietes de l'onglet
    def GetImage(self,iodf):
        img_prop = iodf['Images'][IMAGE].Recopie_data()
        img_prop["suboffset"] = self.i_prop.cb_OffsetSub_Image.GetValue()
        img_prop["DarkOpt"  ] = self.i_prop.cb_optimdark.GetValue()
        img_prop["Stack"    ] = self.i_prop.cb_StackImage.GetValue()
        img_prop["RejType"  ] = self.i_prop.cb_rejtypeImage.GetValue()
        img_prop["RejH"     ] = self.i_prop.e_RejH_Image.GetValue()
        img_prop["RejL"     ] = self.i_prop.e_RejL_Image.GetValue()
        img_prop["Weighted" ] = self.i_prop.cb_weightedImage.GetValue()
        img_prop["Norm"     ] = self.i_prop.cb_NormImage.GetValue()
        img_prop["fwhm"     ] = self.i_prop.e_fwhm_filter.GetValue()
        img_prop["wfwhm"    ] = self.i_prop.e_wfwhm_filter.GetValue()
        img_prop["round"    ] = self.i_prop.e_round_filter.GetValue()
        img_prop["quality"  ] = self.i_prop.e_quality.GetValue()
        img_prop["Cosmetic" ] = self.i_prop.cb_Cosmetic_Image.GetValue()
        img_prop["Hot"      ] = self.i_prop.e_RejH_Cosmetic.GetValue()
        img_prop["Cold"     ] = self.i_prop.e_RejL_Cosmetic.GetValue()
        img_prop["Findstar" ] = self.i_prop.cb_findstar.GetValue()
        img_prop["Ksigma"   ] = self.i_prop.e_sigma_findstar.GetValue()
        img_prop["Roundness"] = self.i_prop.e_roundness_findstar.GetValue()
        img_prop["Degree"   ] = self.i_prop.spDegree.GetValue()
        img_prop["fix_fujix"] = self.i_prop.cb_fix_fujix.GetValue()
        img_prop["Transf"   ] = self.i_prop.cb_transf.GetValue()
        img_prop["NoRot"    ] = self.i_prop.cb_norot.GetValue()
        img_prop["Drizzle"  ] = self.i_prop.cb_drizzle.GetValue()
        img_prop["MinPairs" ] = self.i_prop.e_minpairs.GetValue()
        return img_prop

    # --------------------------------------------------------------------------
    # recupere les proprietes de l'onglet
    def GetOffset(self,iodf):
        offset_prop = iodf['Images'][OFFSET].Recopie_data()
        offset_prop["copylib" ] = self.i_prop.cb_cpylibOffset.GetValue()
        offset_prop["Stack"   ] = self.i_prop.cb_StackOffset.GetValue()
        offset_prop["RejType" ] = self.i_prop.cb_rejtypeOffset.GetValue()
        offset_prop["RejH"    ] = self.i_prop.e_RejH_Offset.GetValue()
        offset_prop["RejL"    ] = self.i_prop.e_RejL_Offset.GetValue()
        offset_prop["Weighted"] = self.i_prop.cb_weightedOffset.GetValue()
        offset_prop["Norm"   ] = self.i_prop.cb_NormOffset.GetValue()
        return offset_prop

    def GetDark(self,iodf):
        dark_prop = iodf['Images'][DARK].Recopie_data()
        dark_prop['suboffset'] = self.i_prop.cb_OffsetSub_Dark.GetValue()
        dark_prop['copylib'  ] = self.i_prop.cb_cpylib_Dark.GetValue()
        dark_prop['Stack'    ] = self.i_prop.cb_StackDark.GetValue()
        dark_prop["RejType"  ] = self.i_prop.cb_rejtypeDark.GetValue()
        dark_prop['RejH'     ] = self.i_prop.e_RejH_Dark.GetValue()
        dark_prop['RejL'     ] = self.i_prop.e_RejL_Dark.GetValue()
        dark_prop["Weighted" ] = self.i_prop.cb_weightedDark.GetValue()
        dark_prop['Norm'     ] = self.i_prop.cb_NormDark.GetValue()
        return dark_prop

    def GetFlat(self,iodf):
        flat_prop = iodf['Images'][FLAT].Recopie_data()
        flat_prop['suboffset'] = self.i_prop.cb_OffsetSub_Flat.GetValue()
        flat_prop['copylib'  ] = self.i_prop.cb_cpylib_Flat.GetValue()
        flat_prop['Stack'    ] = self.i_prop.cb_StackFlat.GetValue()
        flat_prop["RejType"  ] = self.i_prop.cb_rejtypeFlat.GetValue()
        flat_prop['RejH'     ] = self.i_prop.e_RejH_Flat.GetValue()
        flat_prop['RejL'     ] = self.i_prop.e_RejL_Flat.GetValue()
        flat_prop["Weighted" ] = self.i_prop.cb_weightedFlat.GetValue()
        flat_prop['Norm'     ] = self.i_prop.cb_NormFlat.GetValue()
        return flat_prop

    def GetDFlat(self,iodf):
        dflat_prop = iodf['Images'][DFLAT].Recopie_data()
        dflat_prop['suboffset'] = self.i_prop.cb_OffsetSub_DFlat.GetValue()
        dflat_prop['copylib'  ] = self.i_prop.cb_cpylib_DFlat.GetValue()
        dflat_prop['Stack'    ] = self.i_prop.cb_StackDFlat.GetValue()
        dflat_prop["RejType"  ] = self.i_prop.cb_rejtypeDFlat.GetValue()
        dflat_prop['RejH'     ] = self.i_prop.e_RejH_DFlat.GetValue()
        dflat_prop['RejL'     ] = self.i_prop.e_RejL_DFlat.GetValue()
        dflat_prop["Weighted" ] = self.i_prop.cb_weightedDFlat.GetValue()
        dflat_prop['Norm'     ] = self.i_prop.cb_NormDFlat.GetValue()
        return dflat_prop
